#pragma once
#include "Position.h"
#include <string>
using namespace std;

class MapSpot
{
private:
	Position _position;
	bool _active;
	bool _completed;
	MapSpot * _input;
	MapSpot ** _output;// output [] 
	int _outputCount;
	string _name;

	void AddOutput(MapSpot * output);
public:
	MapSpot(Position _p, MapSpot * input, string name);
	MapSpot(Position _p, MapSpot * input, string name, MapSpot ** output, int outputCount);
	MapSpot(const MapSpot & other);

	MapSpot();
	~MapSpot();

	Position getPosition() const;
	void setPosition(const Position & newPosition);

	string getName() const;
	void setName(string value) ;

	bool isActive() const;
	bool isCompleted() const;

	void setActive(bool value = true);
	void setCompleted(bool value = true);
	MapSpot* getOutput(int index);

	MapSpot& operator=(const MapSpot & other);
};

