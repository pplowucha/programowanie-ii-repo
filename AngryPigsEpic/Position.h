#pragma once
class Position
{
private:
	double x, y, z;

public:

	Position(double x=0, double y=0, double z=0);
	~Position();

	double GetX() const;
	double GetY() const;
	double GetZ() const;

	void SetX(double value) ;
	void SetY(double value) ;
	void SetZ(double value) ;
};

