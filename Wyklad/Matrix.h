#pragma once
#include <iostream>
using namespace std;

class Matrix
{
	int _colCount, _rowCount;
	double ** _M;
public:

	Matrix(int rowCount = 3, int colCount = 3);
	Matrix(const Matrix & other);
	~Matrix();

	Matrix& operator=(const Matrix & other);
	Matrix& operator=(double value);

	double *operator[](int i);
	double *operator[](double i);
	Matrix&	operator++(); // ++m  
	Matrix operator++(int); // m++ 

	double& operator ()(int row, int col);

	Matrix operator+(const Matrix & other);
	bool operator>(const Matrix & other);


	friend class MathEngine;

	//  lepiej tak jako funkcje zaprzyjaznione
	friend Matrix operator-(const Matrix & o1, const Matrix & o2);
	friend ostream & operator<<(ostream & out, const Matrix & m);
	friend istream & operator>>(istream & ouint, const Matrix & m);

	friend Matrix operator*(const Matrix & o1, const Matrix & o2);
	friend Matrix operator* (const Matrix & o1, double value);
	friend Matrix operator* (double value, const Matrix & o1);

	friend bool operator==(const Matrix & o1, const Matrix & o2);
	friend bool operator!=(const Matrix & o1, const Matrix & o2);

	operator double();
	
};
//


