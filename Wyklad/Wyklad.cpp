// Wyklad.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;
#include "Matrix.h"
#include "Shape.h"



int _tmain(int argc, _TCHAR* argv[])
{
	//Matrix m1(2, 3);

	//Matrix m2 = m1;
	//
	//m2(1, 1) = 2;
	//m2[0][1] = 1;

	//cout << m2 << endl;


	Shape s1("Shape");
	cout << "Ksztalty" << endl;
	cout << s1 << endl;

	Rectangle r1(2, 4);
	Circle c1( 4);

	cout << r1 << endl;
	cout << c1 << endl;
	

	Shape::shapTypeName = "Ksztalt";
	cout << Shape::GetClassName() << endl;

	cout << Rectangle::GetClassName() << endl;

	//polimorfizm

	Shape & referencja = r1;
	Shape * wskaznik = &c1;

	// dynamiczne wi�zanie
	cout << referencja.Shape::Area() << endl;
	cout << wskaznik->Perimeter() << endl;
	cout << wskaznik->Shape::Perimeter() << endl;

	// virtualny destruktor
	{
		Shape * wskaznik = new Circle(2);

		delete wskaznik;
	}
	system("pause");

	
	return 0;
}

