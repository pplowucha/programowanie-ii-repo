#include "Shape.h"
#define _USE_MATH_DEFINES
#include <math.h>


Shape::Shape(string shapeName)
{
	this->_shapeName = shapeName;
}
Shape::~Shape()
{
}
double Shape::Area()const{
	return 0;
}
double Shape::Perimeter()const{
	return 0;
}
string Shape::shapTypeName; // trzeba r�cznie stworzy� zmienn�

string Shape::GetClassName(){
	return "I\'m Shape";
}

ostream& operator<<(ostream& out, const Shape & other){
	out << (other._shapeName.c_str())<< " " << (other.Area()) << " " << (other.Perimeter());
	return out;
}
ostream& operator<<(ostream& out, const Shape * other){
	out << other->_shapeName.c_str() << " " << other->Area() << " " << other->Perimeter();
	return out;
}

Rectangle::Rectangle(double a, double b) : Shape("Rectangle")
{
	this->_a = a;
	this->_b = b;
	//this->_shapeTypeName = "Rectangle";
}
Rectangle::Rectangle(const Rectangle &other) : Shape("Rectangle")
{
	_a = other._a;
	_b = other._b;
}

Rectangle & Rectangle::operator=(const Rectangle &other)
{
	_shapeName = other._shapeName;
	_a = other._a;
	_b = other._b;

	return *this;
}

Rectangle::~Rectangle()
{
}
double Rectangle::Area()const{
	return _a*_b;
}
double Rectangle::Perimeter()const{
	return 2*(_a+_b);
}
string Rectangle::GetClassName(){
	return "I\'m Rectangle";
}
Circle::Circle(double r) : Shape("Circle")
{
	this->_r = r;
}
Circle::~Circle()
{
}

double Circle::Perimeter()const{
	return 2 * M_PI *_r;
}